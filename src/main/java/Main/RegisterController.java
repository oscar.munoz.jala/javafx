package Main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;


public class RegisterController implements Initializable {
    private String[] roles = {"Profesor", "Estudiante"};
    @FXML
    private ChoiceBox<String> rolChoice;
    @FXML
    private Label lbregistrationMessage;
    @FXML
    private PasswordField pfPassword1;
    @FXML
    private PasswordField pfPassword11;
    @FXML
    private Label lbconfirmPassword;
    @FXML
    private TextField tfName;
    @FXML
    private TextField tfLastName;
    @FXML
    private TextField tfEmail;
    private Stage stage;
    private Scene scene;
    DatabaseConnection connectNow = new DatabaseConnection();
    Connection connectDB = connectNow.getConnection();
    PreparedStatement pStament;
    ResultSet rSet;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        rolChoice.getItems().addAll(roles);
    }

    public void changeScene(String resource, String print, ActionEvent e) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource(resource));
        stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        System.out.println(print);
    }

    public void registerScene(ActionEvent e) throws IOException {
        changeScene("/Main/Register.fxml", "Registrarte", e);
    }

    public void loginScene(ActionEvent e) throws IOException {
        changeScene("/Main/Login.fxml", "Login", e);
    }

    public void inicioScene(ActionEvent e) throws IOException {
        changeScene("/Main/Main.fxml", "Inicio", e);
    }

    //Verifica que los campos de las contraseñas coincidan, invoca y confirma la creación del usuario
    public void registerButtonAction(ActionEvent event) {

        if (rolChoice.getValue() == null ||tfName.getText().isEmpty() || tfLastName.getText().isEmpty() || tfEmail.getText().isEmpty() ||
                pfPassword1.getText().isEmpty() || pfPassword11.getText().isEmpty() ) {

            lbconfirmPassword.setText("Por favor ingrese todos los campos");
        } else if (pfPassword1.getText().equals(pfPassword11.getText())) {
            registerUser();

        } else {
            lbconfirmPassword.setText("Las claves no coinciden.");
            lbregistrationMessage.setText("");
        }
    }

    public void registerUser() {

        try {
            pStament = connectDB.prepareStatement("SELECT count(1) FROM users WHERE email = ?");
            pStament.setString(1,tfEmail.getText());
            rSet = pStament.executeQuery();

            if(rSet.next()) {
                if (rSet.getInt(1) == 1) {
                    lbregistrationMessage.setText("Correo registrado");
                    lbconfirmPassword.setText("");
                } else {
                    System.out.println("No registrado");
                    try {
                        pStament = connectDB.prepareStatement("INSERT INTO users (name, lastName, email, password, rol) VALUES (?,?,?,?,?);");

                        pStament.setString(1, tfName.getText());
                        pStament.setString(2, tfLastName.getText());
                        pStament.setString(3, tfEmail.getText());
                        pStament.setString(4, pfPassword1.getText());
                        pStament.setString(5, rolChoice.getValue());

                        pStament.executeUpdate();
                        lbregistrationMessage.setText("El usuario ha sido registrado exitosamente.");
                        lbconfirmPassword.setText("");
                        connectDB.close();

                    } catch (Exception e) {
                        e.printStackTrace();
                        e.getCause();
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            e.getCause();
        }
    }
}
