package Main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.io.IOException;
import java.sql.*;

public class LoginController {

    @FXML
    private TextField tfEmail;
    @FXML
    private TextField tfName;
    @FXML
    private Label loginMessageLabel;
    @FXML
    private PasswordField pfPassword;
    private Stage stage;
    private Scene scene;
    DatabaseConnection connectNow = new DatabaseConnection();
    Connection connectDB = connectNow.getConnection();
    PreparedStatement pStament;
    ResultSet rSet;

    public void registerScene(ActionEvent e) throws IOException {
        changeScene("/Main/Register.fxml","Registrarte",e);
    }

    public void loginScene(ActionEvent e) throws IOException {
        changeScene("/Main/Login.fxml","Login",e);
    }

    public void inicioScene(ActionEvent e) throws IOException {
        changeScene("/Main/Main.fxml","Inicio",e);
    }

    public void changeScene(String resource,String print,ActionEvent e) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource(resource));
        stage = (Stage)((Node)e.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        System.out.println(print);
    }

    public void loginButtonAction(ActionEvent e) throws IOException {
        if (!tfEmail.getText().isBlank() && !pfPassword.getText().isBlank()){
            if(validateLogin()){
                if (verifyRol().equals("Profesor")){
                    changeScene("/Main/ProfessorProfile.fxml","Interfaz profesor",e);
                    loginMessageLabel.setText(tfEmail.getText()+" inicio sesión");

                }
                else {
                    changeScene("/Main/StudentProfile.fxml","Interfaz estudiante",e);
                    loginMessageLabel.setText("Estudiante inicio de sesión");
                }

            }
            else {
                loginMessageLabel.setText("Correo o contraseña invalida");
            }
        }
        else {
            loginMessageLabel.setText("Hay campos vacios");
        }
    }
    public boolean validateLogin(){

        try {
            pStament = connectDB.prepareStatement("SELECT count(1) FROM users WHERE email = ? AND password = ?");
            pStament.setString(1,tfEmail.getText());
            pStament.setString(2,pfPassword.getText());

            rSet = pStament.executeQuery();

            if (rSet.next()){
                if (rSet.getInt(1)==1){
                    return true;
                }
            }

        } catch (Exception e){
            e.printStackTrace();
            e.getCause();
        }
        return false;
    }

    public String verifyRol(){

        try{
            pStament = connectDB.prepareStatement("SELECT rol FROM users WHERE email = ?");
            pStament.setString(1,tfEmail.getText());
            rSet = pStament.executeQuery();

            if (rSet.next()){
                if (rSet.getString(1).equals("Profesor")){
                    return "Profesor";
                }
            }

        } catch (Exception e){
            e.printStackTrace();
            e.getCause();
        }
        return "";
    }

    public void panelButtonAction(ActionEvent e) throws IOException {
        datesUser();
    }

    public void datesUser(){

        try{
            pStament = connectDB.prepareStatement("SELECT * FROM users WHERE email = '1'");
            rSet = pStament.executeQuery();

            if (rSet.next()){
                tfName.setText(rSet.getString("name"));
            }

        } catch (Exception e){
            e.printStackTrace();
            e.getCause();
        }
    }
}

