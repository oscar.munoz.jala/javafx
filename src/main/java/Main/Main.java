package Main;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Main extends Application {

    private Stage stage;
    private Scene scene;

    @Override
    public void start(Stage Stage) throws Exception {

        try {
            Parent root = FXMLLoader.load(getClass().getResource("/Main/Main.fxml"));
            Scene scene = new Scene(root,1300,900);
            Stage.setScene(scene);
            Stage.show();

        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
    public void registerScene(ActionEvent e) throws IOException {
        changeScene("/Main/Register.fxml","Registrarte",e);
    }

    public void loginScene(ActionEvent e) throws IOException {
        changeScene("/Main/Login.fxml","Login",e);
    }

    public void inicioScene(ActionEvent e) throws IOException {
        changeScene("/Main/Main.fxml","Inicio",e);
    }

    public void changeScene(String resource,String print,ActionEvent e) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource(resource));
        stage = (Stage)((Node)e.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        System.out.println(print);
    }
}
